# Fundamentals of Digital Fabrication


![](../../images/week02/cube_with_holes.jpg)


# Table of contents
- [Project Management](#Paragraph1)
- [2D and 3D Design](#Paragraph2)
	- [2D-Design](#2d-design)



Some introduction text, formatted in heading 2 style


This is the documantation blog for the course I've taken in Winter Semester 2019/2020 at the Rhine-Waal University of Applied Sciences in Kamp-Lintfort.
The Course included the following topics:

## Project management <a name="2d-3d-design"></a>
In this part of the Course we learned to install a collaboration and documentation routine. We are using a GitLab Repository to keep this blog up-to-date and a local Dash console to execute the git commands.

##2D and 3D Design <a name="2d-3d-design"></a>
In this lecture we installed and learnd 2D- and 3D Design Tools.
### 2D-Design
We used the LibreCAD Software because it is free, open source and platform independent. We tried out important functions and created a few sketches. This simple and lightweight tool is very powerful.

*Here Pictures will be inserted*

### 3D-Design <a name="2d-design"></a>



##Laser Cutting
##Electronics Production
##3D Printing
##Electronics Design
##Embedded programming
##Input
##Output
##Final Project


No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## In short

- This website is built automatically by gitlab every time you edit the files in the docs folder
- It does so thanks to [Mkdocs](https://mkdocs.org) a static site generator written in Python
- You must start customizing the file `mkdocs.yml` with your information
- You can change the looks of your website using mkdocs themes, you can find in the `mkdocs.yml` the options for the [Material Mkdocs theme](https://squidfunk.github.io/mkdocs-material/)
- If you want to start from scratch, you can delete everything (using [git-rm](https://git-scm.com/docs/git-rm)) in this repository and push any other static website
